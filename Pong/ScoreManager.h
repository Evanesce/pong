#pragma once
class ScoreManager //singleton class for managing player score (some people don't like this kind of class implementation but I do)
{
public:
	ScoreManager() = default;
	void IncrementPlayer1Score();
	void IncrementPlayer2Score();

	int GetPlayer1Score() const;
	int GetPlayer2Score() const;

	bool isMaxScoreReached() const;

	void reset(); //reset score 

private:

	//score for players 1&2
	int player1Score = 0;
	int player2Score = 0;

	int maxScore = 15;
};

