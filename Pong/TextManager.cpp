#include "TextManager.h"



TextManager::TextManager(sf::RenderWindow* window, ScoreManager* scoreManager, SceneManager* sceneManager)
	:
	window(window),
	scoreManager(scoreManager),
	sceneManager(sceneManager)
{
	if (!arial.loadFromFile("Bin/Fonts/arial.ttf"))
	{
#ifdef _DEBUG
		printf("Could not load arial font!!");
#endif // _DEBUG
	}


	//game over text
	gameOverText.setString("Game Over");
	gameOverText.setCharacterSize(70U);


	//difficulty text
	diffTitle.setString("Difficulty:");
	diffEasy.setString("Easy");
	diffMedium.setString("Medium");
	diffHard.setString("Hard");

	//gamemode text
	gamemodeTitle.setString("Gamemode:");
	singlePlayerText.setString("Single Player");
	multiPlayerText.setString("Multi Player");

	//set location
	player1ScoreText.setPosition(player1ScoreTextLocation);
	player2ScoreText.setPosition(player2ScoreTextLocation);
	difficultyText.setPosition(difficultyTextLocation);

	//difficulty
	diffTitle.setPosition(diffTitlePos);
	diffEasy.setPosition(diffEasyPos);
	diffMedium.setPosition(diffMediumPos);
	diffHard.setPosition(diffHardPos);

	//gamemode
	gamemodeTitle.setPosition(gamemodeTitlePos);
	singlePlayerText.setPosition(singlePlayerPos);
	multiPlayerText.setPosition(multiPlayerPos);


	//gameover
	gameOverText.setPosition(gameOverTextPosition);

	//set Font
	player1ScoreText.setFont(arial);
	player2ScoreText.setFont(arial);
	difficultyText.setFont(arial);
	gameOverText.setFont(arial);
	diffTitle.setFont(arial);
	diffEasy.setFont(arial);
	diffMedium.setFont(arial);
	diffHard.setFont(arial);
	gamemodeTitle.setFont(arial);
	singlePlayerText.setFont(arial);
	multiPlayerText.setFont(arial);



	//set color

	diffEasy.setFillColor(sf::Color::White);
}


//main draw for game scene
void TextManager::Draw() const
{
	DrawScores();
	DrawDiff();
}


//main update for game scene
void TextManager::Update(int aiDiff)
{
	UpdateScore();
	UpdateDifficultyText(aiDiff);
}


//update diff buttons
void TextManager::UpdateDiffScreen(int& aiDiff)
{



	//check and update for easy
	if (diffCollisionCheck(diffEasy))
	{
		diffEasy.setFillColor(sf::Color::Red);
		if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
		{
			sceneManager->advanceScene(1);
			aiDiff = 0;
		}
	}
	else
	{
		diffEasy.setFillColor(sf::Color::White);
	}



	//check and update for medium
	if (diffCollisionCheck(diffMedium))
	{
		diffMedium.setFillColor(sf::Color::Red);
		if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
		{
			sceneManager->advanceScene(1);
			aiDiff = 1;
		}
	}
	else
	{
		diffMedium.setFillColor(sf::Color::White);
	}



	//check and update for hard
	if (diffCollisionCheck(diffHard))
	{
		diffHard.setFillColor(sf::Color::Red);
		if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
		{
			sceneManager->advanceScene(1);
			aiDiff = 2;
		}
	}
	else
	{
		diffHard.setFillColor(sf::Color::White);
	}
}

void TextManager::UpdateGameModeScreen()
{
	//collision for single player
	if (diffCollisionCheck(singlePlayerText))
	{
		singlePlayerText.setFillColor(sf::Color::Red);
		if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
		{
			sceneManager->setGameType(SceneManager::gameType::singlePlayer);
			sceneManager->advanceScene(NULL);
		}
	}
	else
	{
		singlePlayerText.setFillColor(sf::Color::White);
	}
	//collision for multiplayer
	if (diffCollisionCheck(multiPlayerText))
	{
		multiPlayerText.setFillColor(sf::Color::Red);
		if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
		{
			sceneManager->setGameType(SceneManager::gameType::multiPlayer);
			sceneManager->advanceScene(1);
		}
	}
	else
	{
		multiPlayerText.setFillColor(sf::Color::White);
	}
}


//draw game scores
void TextManager::DrawScores() const
{
	window->draw(player1ScoreText);
	window->draw(player2ScoreText);
}


//draw difficulty text
void TextManager::DrawDiff() const
{
	if (sceneManager->getGameType() == SceneManager::gameType::singlePlayer) //stops drawing diff in multiplayer
		window->draw(difficultyText);
}


//draw game over text
void TextManager::DrawGameOver() const
{
	//draw game over text
	window->draw(gameOverText);
}


//draw diff buttons
void TextManager::DrawDiffScreen() const
{
	//draw all diff buttons
	window->draw(diffTitle);
	window->draw(diffEasy);
	window->draw(diffMedium);
	window->draw(diffHard);
}

void TextManager::DrawGameModeScreen() const
{
	window->draw(gamemodeTitle);
	window->draw(singlePlayerText);
	window->draw(multiPlayerText);
}


//update score text
void TextManager::UpdateScore()
{
	std::string player1ScoreString = "Score: " + std::to_string(scoreManager->GetPlayer1Score());
	std::string player2ScoreString = "Score: " + std::to_string(scoreManager->GetPlayer2Score());
	player1ScoreText.setString(player1ScoreString);
	player2ScoreText.setString(player2ScoreString);
}


//update difficulty text
void TextManager::UpdateDifficultyText(int aiDiff)
{

	//setting the string for diff display
	std::string diffString;
	switch (aiDiff)
	{
	case 0:
		diffString = "Easy";
		break;
	case 1:
		diffString = "Medium";
		break;
	case 2:
		diffString = "Hard";
		break;
	}
	difficultyText.setString(diffString);
}


//check for button collision
bool TextManager::diffCollisionCheck(sf::Text inputText) const
{
	//return true if colliding
	return ((sf::Mouse::getPosition(*window).x > inputText.getPosition().x) && ((sf::Mouse::getPosition(*window).x) < (inputText.getPosition().x + inputText.getLocalBounds().width))
		&& (sf::Mouse::getPosition(*window).y > inputText.getPosition().y) && (sf::Mouse::getPosition(*window).y < (inputText.getPosition().y + inputText.getLocalBounds().height)));
}


