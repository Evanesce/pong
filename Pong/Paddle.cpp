#include "Paddle.h"



Paddle::Paddle(sf::RenderWindow* window, int player, SceneManager* sceneManager)
	:
	window(window),
	player(player),
	sceneManager(sceneManager)
{
	paddleBox.setSize(paddleSize);

	ResetPos();
}


void Paddle::Update(sf::Vector2f ballPos, bool BallIsGoing, float ballRadius, int aiDiff)//update logic
{
	//set ball object's is going to paddle is going
	this->isGoing = BallIsGoing;

	//movement update
	switch (this->player)
	{
	case 1:
		paddleBox.setFillColor(sf::Color::Blue);
		Paddle1Movement();
		break;
	case 2:
		Paddle2Movement(ballPos, ballRadius, aiDiff);
		paddleBox.setFillColor(sf::Color::Green);
		break;
	}

	//clamp paddles to screen
	Clamp();

	//set position
	this->paddleBox.setPosition(position);
}


void Paddle::Draw() const //draw to window
{
	window->draw(this->paddleBox);
}


sf::Vector2f Paddle::getPosition() const // basic get
{
	return this->position;
}


int Paddle::getPlayer() const //basic get
{
	return player;
}


void Paddle::Paddle1Movement()//main player movement update
{
	//movement
	if (isGoing)
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{
			this->position.y -= velocity;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			this->position.y += velocity;
		}
	}
	else 
	{
		ResetPos();
	}
}


void Paddle::Paddle2Movement(sf::Vector2f ballPos, float ballRadius, int aiDiff) //AI / second player movement update
{
	switch (sceneManager->getGameType())
	{
	case SceneManager::gameType::singlePlayer:
		switch (aiDiff)
		{
			//easy AI diff
		case 0:
			if (ballPos.x >= window->getSize().x / 2)
			{
				if (ballPos.y <= this->position.y)
				{
					this->position.y -= velocity;
				}

				if (ballPos.y + ballRadius >= this->position.y + paddleSize.y)
				{
					this->position.y += velocity;
				}
			}
			break;

			//medium Ai diff
		case 1:
			if (ballPos.x >= (window->getSize().x / 3))
			{
				if (ballPos.y <= this->position.y + 15)
				{
					this->position.y -= velocity;
				}

				if (ballPos.y + ballRadius >= this->position.y + paddleSize.y - 15)
				{
					this->position.y += velocity;
				}
			}
			break;

			//hard AI Diff
		case 2:
			if (ballPos.y <= this->position.y + (paddleSize.y / 2))
			{
				this->position.y -= velocity;
			}

			if (ballPos.y + ballRadius >= this->position.y + (paddleSize.y / 2))
			{
				this->position.y += velocity;
			}
			break;
		}
		break;
	case SceneManager::gameType::multiPlayer:
		if (isGoing)
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			{
				this->position.y -= velocity;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			{
				this->position.y += velocity;
			}
		}
		else
		{
			ResetPos();
		}
		break;
	}
}


void Paddle::Clamp() //clamp to screen
{
	//clamping
	if (this->position.y < 0)
		this->position.y = 0;
	else if (this->position.y + this->paddleSize.y > window->getSize().y)
		this->position.y = window->getSize().y - paddleSize.y;
}


void Paddle::ResetPos() //reset to middle when !isGoing
{
	//setup starting position
	switch (player)
	{
	case 1:
		this->position = { paddleSidePadding, (window->getSize().y / 2) - (paddleSize.y / 2) };
		break;

	case 2:
		this->position = { window->getSize().x - paddleSize.x - paddleSidePadding, (window->getSize().y / 2) - (paddleSize.y / 2) };
		break;
	}
}

