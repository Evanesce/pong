#pragma once
#include "SceneManager.h"

class SceneManager
{
public:
	SceneManager();


public:
	enum class scene {
		gameModeSelect,
		difficultySelect,
		game,
		end
	};

	enum class gameType {
		singlePlayer,
		multiPlayer
	};
	

public:
	scene getScene() const;
	gameType getGameType() const;
	void advanceScene(int type);

	void setGameType(SceneManager::gameType inputGameMode);

private:


	gameType gameMode;
	scene currentScene = scene::gameModeSelect;
};

