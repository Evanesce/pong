#include "Ball.h"



Ball::Ball(sf::RenderWindow* window, const float radius)
	:
	window(window),
	ballRadius(radius)
{
	//ball setup
	this->position = { (window->getSize().x / 2) - ballRadius * 2, (window->getSize().y / 2) - ballRadius * 2 };
	this->ballCircle.setRadius(ballRadius);
	ballCircle.setFillColor(sf::Color::Cyan);
}


//draw to window
void Ball::Draw() const
{
	window->draw(this->ballCircle);
}


//update logic
void Ball::Update(Paddle* paddle1, Paddle* paddle2, ScoreManager* scoreManager)
{
	//start from middle
	if(!isGoing)
	{
		StartGoing();
	}
	else
	{
		//worst case fix
		if (this->velocity.x == 0)
			this->velocity.x = 1;



		//moving
		position += velocity;


		//clamp and collision
		ClampWalls();
		ClampPaddle(paddle1);
		ClampPaddle(paddle2);
		

		//scoring
		if (this->position.x + ballRadius * 2 > window->getSize().x) //player1 scores
		{
			scoreManager->IncrementPlayer1Score();
			isGoing = false;
		}
		if (this->position.x < 0) //player 2 scores
		{
			scoreManager->IncrementPlayer2Score();
			isGoing = false;
		}

	}

	//update pos
	this->ballCircle.setPosition(this->position);
}


//simple get
sf::Vector2f Ball::getPosition() const
{
	return this->position;
}


//simple get to pass through to paddles
bool Ball::getIsGoing() const
{
	return this->isGoing;
}


//starts the game moving
void Ball::StartGoing()
{	
	//set random velocities
	while (true)
	{
		std::mt19937::result_type seed = static_cast<unsigned>(std::chrono::high_resolution_clock::now().time_since_epoch().count());
		auto velX = std::bind(std::uniform_int_distribution<int>(-4, 4), std::mt19937(seed));
		auto velY = std::bind(std::uniform_int_distribution<int>(-4, 4), std::mt19937(seed));
		if (velX() != 0)
		{
			this->velocity = { static_cast<float>(velX()), static_cast<float>(velY()) };
			break;
		}
	}

	//sets ball to middle
	this->position = { (window->getSize().x / 2) - ballRadius * 2, (window->getSize().y / 2) - ballRadius * 2 };
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
		isGoing = true;
}


//clamps balls to the paddles (collision)
void Ball::ClampPaddle(Paddle * paddle)
{
	//paddle Clamping

	//paddle 1
	if ((paddle->getPlayer() == 1) && (this->position.y > paddle->getPosition().y - 5) && (this->position.y + ballRadius * 2 < paddle->getPosition().y + paddle->paddleSize.y + 5))
	{
		if ((this->position.x <= paddle->getPosition().x + paddle->paddleSize.x) && this->position.x > paddle->getPosition().x)
		{
			this->velocity.x = -this->velocity.x + 0.2f;
			StepVelocity();
		}
	}

	//paddle 2
	if ((paddle->getPlayer() == 2) && (this->position.y > paddle->getPosition().y - 5) && (this->position.y + ballRadius * 2 < paddle->getPosition().y + paddle->paddleSize.y + 5))
	{
		if ((this->position.x + ballRadius*2 >= paddle->getPosition().x) && this->position.x < paddle->getPosition().x + paddle->paddleSize.x)
		{
			this->velocity.x = -this->velocity.x - 0.2f;
			StepVelocity();
		}
	}
}


//clamp ball to roof and floor
void Ball::ClampWalls()
{
	//vertical clamping
	if ((this->position.y < 0) || (this->position.y + ballRadius * 2 >= window->getSize().y))
	{
		this->velocity.y = -this->velocity.y;
		StepVelocity();
	}

}


//incremental speed increase to ball after every hit
void Ball::StepVelocity()
{
	if (this->velocity.x < 0)
	{
		this->velocity.x -= velocityStepAmount.x;
	}
	else if  (this->velocity.x > 0)
	{
		this->velocity.x += velocityStepAmount.x;
	}

	if (this->velocity.y < 0)
	{
		this->velocity.y -= velocityStepAmount.y;
	}
	else if (this->velocity.y > 0)
	{
		this->velocity.y += velocityStepAmount.y;
	}
}

