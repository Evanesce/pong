#include "SceneManager.h"



SceneManager::SceneManager()
{
}

SceneManager::scene SceneManager::getScene() const
{
	return currentScene;
}

SceneManager::gameType SceneManager::getGameType() const
{
	return gameMode;
}

void SceneManager::advanceScene(int type)
{
	if (type == 1)
	{
		switch (currentScene)
		{
		case scene::difficultySelect:
			currentScene = scene::game;
			break;
		case scene::game:
			currentScene = scene::end;
			break;
		case scene::gameModeSelect:
			currentScene = scene::game;
			break;
		}
	}
	else if (type == 3)
	{
		currentScene = scene::gameModeSelect;
	}
	else
	{
		currentScene = scene::difficultySelect;
	}
}

void SceneManager::setGameType(SceneManager::gameType inputGameMode)
{
	this->gameMode = inputGameMode;
}


