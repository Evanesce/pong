#pragma once
#include <SFML/Graphics.hpp>
#include "Paddle.h"
#include "Ball.h"
#include "TextManager.h"
#include "SceneManager.h"
#include "ScoreManager.h"



//TODO:
/*
sort out Text for game mode select screen
sort out collision for game mode select
sort out logic for game mode select

*/

class Game
{
public:
	Game(sf::RenderWindow* window);
	void Go();


private:
	void ComposeFrame(); //draw to window
	void UpdateModel(); //update game logic
	void GameSceneDraw() const;

	void resetGame(); //resets game state

private:
	//game window
	sf::RenderWindow* window;

	//multi class variables
	const float ballRadius = 15.0f;
	int aiDiff = 2;



	//game objects
	Paddle player1Paddle;
	Paddle player2Paddle;
	Ball gameBall;

	//management objects
	ScoreManager scoreManager;
	TextManager textManager;
	SceneManager sceneManager;


};

