#pragma once
#include <SFML/Graphics.hpp>
#include <string>
#include "ScoreManager.h"
#include "SceneManager.h"

class TextManager
{
public:
	TextManager(sf::RenderWindow* window, ScoreManager* scoreManager, SceneManager* sceneManager);
	

	//main draw and update functions
	void Draw() const;
	void DrawGameOver() const;
	void DrawDiffScreen() const;
	void DrawGameModeScreen() const;
	void Update(int aiDiff);
	void UpdateDiffScreen(int& aiDiff);
	void UpdateGameModeScreen();

private:
	//sub draw and update functions
	void DrawScores() const;
	void DrawDiff() const;
	void UpdateScore();
	void UpdateDifficultyText(int aiDiff);
	bool diffCollisionCheck(sf::Text inputText) const;


private:
	//class pointers
	sf::RenderWindow* window;
	ScoreManager* scoreManager;
	SceneManager* sceneManager;

	//diff screen text
	sf::Text diffTitle;
	sf::Text diffEasy;
	sf::Text diffMedium;
	sf::Text diffHard;

	//in Game Text
	sf::Text player1ScoreText;
	sf::Text player2ScoreText;
	sf::Text difficultyText;

	//end game text
	sf::Text gameOverText;

	//gamemode text
	sf::Text gamemodeTitle;
	sf::Text singlePlayerText;
	sf::Text multiPlayerText;

	//fonts
	sf::Font arial;

	//positions
	//score text
	const sf::Vector2f player1ScoreTextLocation = { static_cast<float>(window->getSize().x / 10), 40.0f };
	const sf::Vector2f player2ScoreTextLocation = { static_cast<float>(window->getSize().x / 10) * 8, 40.0f };;
	const sf::Vector2f difficultyTextLocation = { static_cast<float>(window->getSize().x / 2 - 50), 40.0f };

	//game over
	sf::Vector2f gameOverTextPosition = { static_cast<float>((window->getSize().x / 2) - (170)), static_cast<float>((window->getSize().y / 2) - (50)) };

	//difficulty buttons
	const sf::Vector2f diffTitlePos = { static_cast<float>(window->getSize().x / 5), 40.0f };
	const sf::Vector2f diffEasyPos = { static_cast<float>(window->getSize().x / 5)*2, 40.0f };
	const sf::Vector2f diffMediumPos = { static_cast<float>(window->getSize().x / 5)*3, 40.0f };
	const sf::Vector2f diffHardPos = { static_cast<float>(window->getSize().x / 5)*4, 40.0f };


	//game mode buttons
	const sf::Vector2f gamemodeTitlePos = { static_cast<float>(window->getSize().x / 5), 40.0f };
	const sf::Vector2f singlePlayerPos = { static_cast<float>(window->getSize().x / 5) * 2, 40.0f };
	const sf::Vector2f multiPlayerPos = { static_cast<float>(window->getSize().x / 5) * 3, 40.0f };
};

