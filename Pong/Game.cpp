#include "Game.h"



Game::Game(sf::RenderWindow* window)
	:
	window(window),
	player1Paddle(window, 1, &sceneManager),
	player2Paddle(window, 2, &sceneManager),
	gameBall(window, ballRadius),
	textManager(window, &scoreManager, &sceneManager)
{
}


void Game::Go()
{
	window->clear();
	/////////////////////////
	UpdateModel();
	ComposeFrame();
	/////////////////////////
	window->display();
}


void Game::ComposeFrame()//draw to window
{
	switch (sceneManager.getScene())
	{
		//mode screen
	case SceneManager::scene::gameModeSelect:
		textManager.DrawGameModeScreen();
		break;

	//difficulty screen
	case SceneManager::scene::difficultySelect:
		textManager.DrawDiffScreen();
		break;

		//game screen
	case SceneManager::scene::game:
		if (scoreManager.isMaxScoreReached())
		{
			sceneManager.advanceScene(1);
		}
		GameSceneDraw();
		break;

		//game over screen
	case SceneManager::scene::end:
		GameSceneDraw();
		textManager.DrawGameOver();
		break;
	}
}


void Game::UpdateModel() //update game logic
{
	switch (sceneManager.getScene())//test for score limit
	{
	case SceneManager::scene::game:
		//reset check
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		{
			sceneManager.advanceScene(3);
			resetGame();
		}


		//update paddles
		player1Paddle.Update(gameBall.getPosition(), gameBall.getIsGoing(), ballRadius, aiDiff);
		player2Paddle.Update(gameBall.getPosition(), gameBall.getIsGoing(), ballRadius, aiDiff);

		//update ball
  		gameBall.Update(&player1Paddle, &player2Paddle, &scoreManager);

		//update Text
		textManager.Update(aiDiff);
		break;

		//updates for the diff screen
	case SceneManager::scene::difficultySelect:
		textManager.UpdateDiffScreen(aiDiff);
		break;
		//update for mode screen
	case SceneManager::scene::gameModeSelect:
		textManager.UpdateGameModeScreen();
		break;
	}
}

void Game::GameSceneDraw() const
{
	//draw ball
	gameBall.Draw();

	//draw paddles
	player1Paddle.Draw();
	player2Paddle.Draw();

	//draw text
	textManager.Draw();
}

void Game::resetGame()
{
	gameBall.reset();
	scoreManager.reset();
}
