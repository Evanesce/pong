#include "ScoreManager.h"



void ScoreManager::IncrementPlayer1Score() //increments player 1 score
{
	player1Score++;
}


void ScoreManager::IncrementPlayer2Score() //increments player 2 score
{
	player2Score++;
}

int ScoreManager::GetPlayer1Score() const
{
	return player1Score;
}

int ScoreManager::GetPlayer2Score() const
{
	return player2Score;
}

bool ScoreManager::isMaxScoreReached() const
{
	return (player1Score >= 15 || player2Score >= 15);
}

void ScoreManager::reset()
{
	player1Score = 0;
	player2Score = 0;
}
