#pragma once
#include <SFML/Graphics.hpp>
#include "SceneManager.h"

class Paddle
{
public:
	Paddle(sf::RenderWindow *window, int player, SceneManager* sceneManager);

	//public use functions
	void Update(sf::Vector2f ballPos, bool isGoing, float ballRadius, int aiDiff);//update logic
	void Draw() const;//draw to window
	sf::Vector2f getPosition() const;//basic get
	int getPlayer() const; //basic get


public:
	//public variables
	const sf::Vector2f paddleSize = { 30.0f, 160.0f };

private:
	//private functions
	void Paddle1Movement();//always active player
	void Paddle2Movement(sf::Vector2f ballPos, float ballRadius, int aiDiff);//AI / second player (not added yet)
	void Clamp();//clamp to roof and floor
	void ResetPos();//reset when ball isn't Active

private:


	//window
	sf::RenderWindow* window;
	int player;
	bool isGoing;

	//paddle variables
	sf::RectangleShape paddleBox;
	const float velocity = 5.0f;


	//position variables
	sf::Vector2f position;
	static constexpr float paddleSidePadding = 70.0f;

	SceneManager* sceneManager = nullptr;
};

