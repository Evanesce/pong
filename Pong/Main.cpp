#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include "Game.h"

#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

int main()
{
	//main window
	sf::RenderWindow window(sf::VideoMode(1280, 720), "Pong", sf::Style::Close | sf::Style::Titlebar);
	window.setFramerateLimit(210); //making lives so much easier since 29/06/2017
	//window.setVerticalSyncEnabled(true);

	//setup game object
	Game theGame(&window);


	//game loop
	while (window.isOpen())
	{
		//event handler for close
		sf::Event event;
		while (window.pollEvent(event))
		{
			//event switch for close
			switch (event.type)
			{
				//close window
			case sf::Event::Closed:
				window.close();
				break;
			}
		}

		/////////////
		if (window.hasFocus())
			theGame.Go();
		/////////////
	}

	return EXIT_SUCCESS;
}