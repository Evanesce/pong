#pragma once
#include <SFML/Graphics.hpp>
#include "Paddle.h"
#include "ScoreManager.h"
#include <random>
#include <chrono>
#include <functional>

class Ball
{
public:
	Ball(sf::RenderWindow* window, const float radius);

	void Draw() const;//draw to window
	void Update (Paddle* paddle1, Paddle* paddle2, ScoreManager* scoreManager);//update logic
	sf::Vector2f getPosition() const;//basic get
	bool getIsGoing() const;//basic get

	void reset() { isGoing = false; }; //reset ball pos

private:
	void StartGoing();//starts game movement
	void ClampPaddle(Paddle* paddle);//collision for paddles
	void ClampWalls();//collision for roof and floor
	void StepVelocity();//incremental speed incr

private:

	sf::RenderWindow* window;


	//ball variables
	sf::CircleShape ballCircle;
	const float ballRadius;
	bool isGoing = false;


	//velocity
	sf::Vector2f velocity = { -2.0f, 4.0f };
	sf::Vector2f position;
	const sf::Vector2f velocityStepAmount = { 0.1f, 0.1f };
};

